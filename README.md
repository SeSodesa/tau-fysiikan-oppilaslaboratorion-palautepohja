# TAU Fysiikan oppilaslaboratorion palautepohja

Tästä hakemistosta löytyviin palautepohjiin `palaute.tex` ja
`feedback.tex` voi täyttää lukukohtaiset palautteet koskien
Tampereen yliopiston kurssin *Fysiikan laboratoriotyöt*
raporttien palautuksia.

## Palautteen kääntäminen PDF-muotoon…

… tapahtuu kääntäjällä `pdflatex`, joka tulee jokaisen modernin
LaTeX-jakelun kuten [TeX Live](https://tug.org/texlive/) mukana.
Kääntämiseen tarvittava komento on
```sh
pdflatex palaute.tex && biber palaute && pdflatex palaute.tex ,
```
tai englanninkielisessä tapauksessa
```sh
pdflatex feedback.tex && biber feedback && pdflatex feedback.tex .
```
Näissä listauksissa komento `biber palaute` tai `biber feedback` *ilman tiedostopäätettä*
sisällyttää tiedostossa `lahteet.bib` olevia lähteitä palautteen lähdeluetteloon,
jos niihin on viitattu jossakin päin palautetta komennolla `\cite{lähteen-id}`.
Täältä löytyvät esimerkiksi malliselostus sekä virhearvio-ohjeet.

## Vinkki Gitin käyttäjille

Jos olet ladannut tämän projektin koneellesi versionhallintaohjelmiston Git kautta,
on projektin peruuttaminen sen perustilaan erittäin kätevää. Tietylle opiskelijaparille
kirjoitetun palautteen antamisen jälkeen voi kaikki syötteet poistaa komennolla
```sh
git checkout -- . ,
```
jonka jälkeen voit alkaa kirjoittaa palautetta seuraavalle parille.
Tämä poistaa tarpeen pohjaan syötettyjen palautteiden käsin poistamiselle
ennen seuraavan palauutteen kirjoittamisen aloittamista.
